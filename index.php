
<?php

date_default_timezone_set("Europe/Berlin");
spl_autoload_register(function($class){
    include "{$class}.php";
});
session_start();

use Models\User;

define('ROOT', str_replace("\\",'/',__DIR__));

require_once ROOT.'/functions.php';

$request = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '/';
$user = null;
if(isset($_COOKIE['user'])){
    $user = User::getBy($_COOKIE['user']);
    if(is_null($user)){
        logout();
    }
    if(!isset($_SESSION['user'])){
        $_SESSION['user'] = $user;
    }
}

switch ($request) {

    case '/delete' :
        require 'delete.php';
        break;

    case '/home' :
    case '/' :
    case '' :
        require 'views/home.php';
        break;

    case '/login' :
        require 'views/login.php';
        break;

    case '/subscribe' :
    case '/register' :
        require 'views/register.php';
        break;

    case '/detail' :
    case '/announce':
        require 'views/user/detail-announce.php';
        break;

    case '/announces' :
    case '/search' :
        require 'views/user/search-announce.php';
        break;

    case '/me' :
    case '/profile' :
    case '/user' :
    case '/reservations' :
        require 'views/user/profile.php';
        break;

    case '/add' :
    case '/new' :
        require 'views/announcer/add-announce.php';
        break;

    case '/logout' :
        $_GET['logout'] = 1;
        require 'views/user/profile.php';
        break;

    default:
        http_response_code(404);
        require '404.php';
        break;
}