/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE IF NOT EXISTS `airbnb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `airbnb`;

CREATE TABLE IF NOT EXISTS `announce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `announcer_id` int(11) NOT NULL,
  `city` varchar(256) NOT NULL,
  `country` varchar(56) NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `type` enum('whole','private','collocation') NOT NULL DEFAULT 'whole',
  `surface` varchar(56) NOT NULL,
  `places` int(11) NOT NULL DEFAULT '1',
  `title` varchar(56) NOT NULL,
  `description` text NOT NULL,
  `authorize_animals` tinyint(1) NOT NULL,
  `authorize_smokers` tinyint(1) NOT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `announce_user_id_fk` (`announcer_id`),
  CONSTRAINT `announce_user_id_fk` FOREIGN KEY (`announcer_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40000 ALTER TABLE `announce` DISABLE KEYS */;
/*!40000 ALTER TABLE `announce` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `announce_id` int(11) NOT NULL,
  `start_timestamp` timestamp NOT NULL,
  `end_timestamp` timestamp NOT NULL,
  `places` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contract_announce_id_fk` (`announce_id`),
  KEY `contract_user_id_fk` (`user_id`),
  CONSTRAINT `contract_announce_id_fk` FOREIGN KEY (`announce_id`) REFERENCES `announce` (`id`) ON DELETE CASCADE,
  CONSTRAINT `contract_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40000 ALTER TABLE `contract` DISABLE KEYS */;
/*!40000 ALTER TABLE `contract` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `announce_id` int(11) NOT NULL,
  `name` varchar(56) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `equipment_announce_id_fk` (`announce_id`),
  CONSTRAINT `equipment_announce_id_fk` FOREIGN KEY (`announce_id`) REFERENCES `announce` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `announce_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `favorite_announce_id_fk` (`announce_id`),
  KEY `favorite_user_id_fk` (`user_id`),
  CONSTRAINT `favorite_announce_id_fk` FOREIGN KEY (`announce_id`) REFERENCES `announce` (`id`) ON DELETE CASCADE,
  CONSTRAINT `favorite_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40000 ALTER TABLE `favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorite` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `permission_user_id_fk` (`user_id`),
  KEY `permission_permission_type_id_fk` (`type_id`),
  CONSTRAINT `permission_permission_type_id_fk` FOREIGN KEY (`type_id`) REFERENCES `permission_type` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `permission_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='possible types of permissions';

/*!40000 ALTER TABLE `permission_type` DISABLE KEYS */;
INSERT INTO `permission_type` (`id`, `name`) VALUES
	(1, 'standard'),
	(2, 'announcer'),
	(3, 'administrator');
/*!40000 ALTER TABLE `permission_type` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `announce_id` int(11) NOT NULL,
  `start_timestamp` timestamp NOT NULL,
  `end_timestamp` timestamp NOT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `requested_places` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation_announce_id_fk` (`announce_id`),
  KEY `reservation_user_id_fk` (`user_id`),
  CONSTRAINT `reservation_announce_id_fk` FOREIGN KEY (`announce_id`) REFERENCES `announce` (`id`) ON DELETE CASCADE,
  CONSTRAINT `reservation_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) NOT NULL,
  `password` varchar(64) NOT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `first_name` varchar(56) NOT NULL,
  `last_name` varchar(56) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='utilisateur';

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
