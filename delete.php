<?php

use Models\Announce;
use Models\Reservation;
use Models\User;
use Models\Favorite;
use Models\Contract;
use Core\Database;

if(!isset($_SESSION['user'])){
    alert("You must be connected.", 'warning', ['Login' => '/login']);
    return;
}

if(!isset($_GET['type'],$_GET['id'])){
    alert("You have not defined what you want to delete.", 'danger', ['Back to home' => '/home']);
    return;
}

$id = $_GET['id'];

switch($_GET['type']){

    case 'favorite' :
        $favorite = Favorite::get(intval($id));
        if(is_null($favorite)){
            alert("This favorite does not exist.", 'danger', ['Back to profile' => '/me']);
            return;
        }
        if($favorite['user_id'] !== $_SESSION['user']['id']){
            alert("This favorite does not belong to you", 'danger', ['Back to home' => '/home']);
            return;
        }
        Favorite::delete(intval($id));
        alert("Your favorite has been deleted", 'success', ['Back to profile' => '/me']);
        break;

    case 'profile' :
    case 'me' :
        if($id !== $_SESSION['user']['id'] && !in_array('administrator',$_SESSION['permissions'])){
            alert("You cannot delete a profile that does not belong to you.", 'danger', ['Back to home' => '/home']);
            return;
        }
        User::delete(intval($id));
        alert("The profile has been deleted", 'success', ['Back to home' => '/home']);
        break;

    case 'announce' :
        $announce = Announce::get(intval($id));
        if(is_null($announce)){
            alert("This announce does not exist.", 'danger', ['Back to profile' => '/me']);
            return;
        }
        if($announce['announcer_id'] !== $_SESSION['user']['id']){
            alert("This announce does not belong to you", 'danger', ['Back to home' => '/home']);
            return;
        }
        Announce::delete(intval($id));
        alert("Your announce has been deleted", 'success', ['Back to profile' => '/me']);
        break;

    case 'reservation' :
    case 'book' :
        $reservation = Reservation::get(intval($id));
        if(is_null($reservation)){
            alert("This reservation does not exist.", 'danger', ['Back to profile' => '/me']);
            return;
        }
        if($reservation['user_id'] !== $_SESSION['user']['id'] && $reservation['announcer_id'] !== $_SESSION['user']['id']){
            alert("This reservation does not belong to you", 'danger', ['Back to home' => '/home']);
            return;
        }
        Reservation::delete(intval($id));
        if(isset($_GET['contract'])){
            $success = Contract::add([
                'user_id' => $reservation['user_id'],
                'announce_id' => $reservation['announce_id'],
                'start_timestamp' => $reservation['start_timestamp'],
                'end_timestamp' => $reservation['end_timestamp'],
                'places' => $reservation['requested_places']
            ]);
            alert("The reservation has been accepted", 'success', ['Back to profile' => '/me']);
        }else{
            alert("The reservation has been deleted", 'success', ['Back to profile' => '/me']);
        }
        break;

    default :
        alert("The deletion type is incorrect", 'danger', ['Back to home' => '/home']);
        break;

}

