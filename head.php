<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="/src/css/bootstrap.min.css">
<link rel="stylesheet" href="/src/css/style.css">
<script src="/src/js/jquery-3.4.1.min.js"></script>
<script src="/src/js/bootstrap.min.js"></script>
<script src="/src/js/script.js"></script>
<link rel="shortcut icon" href="/src/img/airbnb.svg" type="image/x-icon">