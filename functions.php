<?php

use Core\Database;

function alert($message, $color = 'primary', $buttons = [] ){
    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php include_once ROOT.'/head.php' ?>
        <title> AirBnB | Alert </title>
    </head>
    <body class="d-flex flex-column align-items-center justify-content-center min-vh-100">
        <?php
            echo "<div class='alert alert-$color'> $message </div>";
            foreach($buttons as $text => $url){
                echo  "<a class='btn btn-primary m-1 text-light' href='$url'> $text </>";
            }
        ?>
    </body>
    </html>
    <?php
}

function rand_image($keyword = 'building,village,house'){
    echo "https://loremflickr.com/320/240/{$keyword}?rand=".strval(rand(1,1000));
}

function full_name( array $user ): string
{
    return ucwords(strtolower($user['first_name'].' '.$user['last_name']));
}

function period( $reservation ): string
{
    return 'from <span class="badge badge-info">'._date($reservation['start_timestamp']).'</span><br> to <span class="badge badge-info">'._date($reservation['end_timestamp']).'</span>';
}

function _date( string $date ){
    return '<span class="badge badge-info">'.date('Y / m / d',strtotime($date)).'</span>';
}

function logout(){
    setcookie('user', '', time() - 3600);
    unset($_COOKIE['user']);
    if(isset($_SESSION['user'])){
        unset($_SESSION['user']);
    }
}

function card( string $title, array $table, string $button = null ){
    ?>
    <div class="card shadow m-2">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center mb-3">
                <h2 class="card-title text-info"> <?php echo $title ?> </h2>
                <?php if(!is_null($button)): ?>
                    <button class="btn btn-info ml-3"><a href="/<?php echo $button ?>"> <?php echo ucfirst($button) ?> </a></button>
                <?php endif; ?>
            </div>
            <table class="card-text">
                <?php
                foreach($table as $row){
                    echo '<tr>';
                    foreach($row as $data => $head){
                        if($head){
                            echo "<th class='px-3'> $data </th>";
                        }else{
                            echo "<td class='px-3'> $data </td>";
                        }
                    }
                    echo '</tr>';
                }
                ?>
            </table>
        </div>
    </div>
    <?php
}

function beautify( string $input ): string
{
    return ucwords(str_replace('_',' ',$input));
}

function input( $type, $name = null, $params = []){
    if(empty($name)) $name = $type;
    if($type == 'number') {
        ?>
        <label>
            <span class="text-secondary"> <?php echo beautify($name) ?> : </span>
            <input type="<?php echo $type ?>" step="0.01" name="<?php echo $name ?>"
                   class="rounded shadow border-0 p-2 m-1" <?php value($name) ?> required>
            <?php if($name == 'price'){ ?><span class="badge badge-info"> $/month </span> <?php } ?>
        </label>
        <?php
    }else if($type == 'checkbox'){
        ?>
         <label>
            <span class="text-secondary"> <?php echo beautify($name) ?> : </span>
            <input type="<?php echo $type ?>" name="<?php echo $name ?>"
                   class="p-2 m-1">
        </label>
        <?php
    }else if($type == 'radio'){
        echo '<div>';
        foreach ($params as $param) {
            ?>
            <label>
                <span class="text-secondary"> <?php echo beautify($param) ?> </span>
                <input type="<?php echo $type ?>" name="<?php echo $name ?>"
                       class="p-2 m-1" value="<?php echo $param ?>" required>
            </label>
            <?php
        }
        echo '</div>';
    }else if($type == 'textarea') {
        ?>
        <label class="d-flex flex-column w-100">
            <?php if(isset($params['notice'])) echo '<p class="text-secondary">'.$params['notice'].'</p>' ?>
            <textarea name="<?php echo $name ?>" id="<?php echo $params['form'] ?>" cols="20" rows="10"
                class="rounded shadow border-0 p-4 m-1" required
                placeholder="<?php echo beautify($name).'...' ?>"></textarea>
        </label>
        <?php
    }else if($type == 'submit'){
        ?>
        <input type="submit" name="<?php echo $name ?>" value="<?php echo beautify($name) ?>" class="btn btn-primary m-2">
        <?php
    }else{
        ?>
        <input type="<?php echo $type ?>" name="<?php echo $name ?>" placeholder="<?php echo beautify($name) ?>"
               class="rounded shadow border-0 p-2 m-1" <?php value($name); if($type != 'search') echo 'required' ?>>
        <?php
    }
}

function value( string $name ){
    echo isset($_POST[$name]) ? 'value="'.$_POST[$name].'"' : '';
}

function includes( string $pattern, string $text ): bool
{
    return strpos(strtoupper($text),strtoupper($pattern)) !== false;
}