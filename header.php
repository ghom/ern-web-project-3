<header class="bg-dark text-light p-3 shadow-lg">
    <div class="container d-flex align-items-center justify-content-between">
        <div class="d-flex align-items-center">
            <img src="/src/img/airbnb.svg" alt="logo" height="60"">
            <h1 class="display-4 px-3"><a href="/home"> AirBnB </a></h1>
        </div>
        <?php
        if(isset($_SESSION['user'])){
            ?>
            <form action="/search" method="get">
                <?php input('search'); ?>
            </form>
            <div class="btn-group" role="group">
                <a class="btn btn-danger text-light" href="/login?logout=1">
                    Logout
                </a>
            <?php
            if(isset($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'] == '/profile'){
                ?>
                <a class="btn btn-light text-dark" href="/home">
                    Home
                </a>
                <?php
            }else{
                ?>
                <a class="btn btn-light text-dark" href="/me">
                    <?php echo full_name($_SESSION['user']) ?>
                </a>
                <?php
            }
            ?>
            </div>
            <?php
        }else{
            ?>
            <div class="btn-group" role="group">
                <a class="btn btn-secondary text-light" href="/register"> Register </a>
                <a class="btn btn-light text-dark" href="/login"> Login </a>
            </div>
            <?php
        }
        ?>

    </div>
</header>
