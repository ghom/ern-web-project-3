<?php

namespace Core;

use Exception;
use PDO;
use PDOStatement;

abstract class Database
{
    private const DB_USED = 'mysql';
    private const DB_NAME = 'airbnb';
    private const DB_HOST = 'localhost';
    private const DB_USER = 'root';
    private const DB_PASS = '';

    protected static $connexion = null;

    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}

    public static function connexion(): PDO
    {
        if(is_null(self::$connexion)){
            try{
                self::$connexion = new PDO(
                    self::DB_USED.':dbname='.self::DB_NAME.';host='.self::DB_HOST,
                    self::DB_USER, self::DB_PASS
                );
                self::$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }catch(Exception $error){
                echo( $error->getMessage() );
            }
        }
        return self::$connexion;
    }

    public static function query( string $query ): ?PDOStatement
    {
        $stmt = null;
        try { $stmt = self::connexion()->query($query); }
        catch(Exception $error){ die($error->getMessage()); }
        return $stmt;
    }

    public static function fetch( string $query ): ?array
    {
        $stmt = self::query($query);
        if(is_null($stmt)) return null;
        $item = $stmt->fetch(PDO::FETCH_ASSOC);
        if(!$item) return null;
        return $item;
    }

    public static function fetchAll( string $query ): ?array
    {
        $stmt = self::query($query);
        if(is_null($stmt)) return null;
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function set( string $query ): bool
    {
        return !is_null(self::query($query));
    }

    public static function has( string $query ): bool
    {
        $rows = self::fetchAll($query);
        return count($rows) > 0;
    }

    public static function exists( string $table, int $id ): bool
    {
        return self::has("SELECT id FROM $table WHERE id = $id");
    }

    public static function insert( string $table, array $data ): bool
    {
        $keys = join(', ', array_keys($data));
        $dots = join(', ',str_split(str_repeat('?', count($data))));
        $query = "INSERT INTO $table ( $keys ) VALUES ( $dots )";

        try{
            $stmt = self::connexion()->prepare($query);
            return $stmt->execute(array_values($data));
        }catch(Exception $error){
            die($error->getMessage());
        }
    }
}


