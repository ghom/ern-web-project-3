<?php

use Models\User;

if(isset($_GET['logout'])){
    logout();
    alert("You are successfully disconnected.", "success", ["Back to login" => '/login']);
    return;
}

if(!empty($_COOKIE['user'])){
    alert("You are already connected.", "warning", ["Back to home page" => '/home']);
    return;
}

$error = false;
if(!empty($_POST['login'])){
    if(!empty($_POST['email'])){
        $email = $_POST['email'];
        if($user = User::getBy($email,'email',"'")){
            if(!empty($_POST['password'])){
                $password = hash('sha256', $_POST['password']);
                if($user['password'] === $password){
                    setcookie('user', $user['id'], time()+3600*24*7);
                    $_SESSION['user'] = $user;
                    alert('You are successfully logged in !', 'success', ['Back to website' => '/home']);
                    return;
                }else{
                    $error = 'Your password is incorrect';
                }
            }else{
                $error = 'You must enter a password';
            }
        }else{
            $error = 'The email entered is incorrect';
        }
    }else{
        $error = 'You must enter your email';
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php include_once ROOT.'/head.php' ?>
    <title> AirBnB | Login </title>
</head>
<body class="d-flex justify-content-center align-items-center flex-column">
    <form action="/login" method="POST" class="d-flex flex-column align-items-center mt-3 w-50 h-50">
        <img src="/src/img/airbnb.svg" alt="logo" height="100"">
        <h1 class="display-3 mb-4 text-secondary"> Login </h1>
        <?php
        if($error){
            echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
        }
        input('email');
        input('password');
        input('submit', 'login');
        ?>
    </form>
    <a class="btn btn-secondary text-light" href="/register"> Create account </>
</body>
</html>