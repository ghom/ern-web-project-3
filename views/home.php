<?php

use Models\Announce;

$announces = null;
if(isset($_COOKIE['user'])){
    $announces = Announce::getLastAnnounces(6);
}

?><!doctype html>
<html lang="en">
<head>
    <?php include_once ROOT.'/head.php' ?>
    <title> AirBnB | Home </title>
</head>
<body class="min-vh-100 d-flex flex-column">
    <?php include ROOT.'/header.php' ?>
    <main class="flex-grow-1">
        <div class="container">
            <h1 class="display-3 mt-3"> Welcome to AirBnB </h1>
            <blockquote class="blockquote p-4">
                <p> Best real estate rental site in the world... <br> Your vacation will finally be profitable! </p>
                <footer class="blockquote-footer"> Ghom, el patron </footer>
            </blockquote>
            <?php
            if(isset($announces)){
                echo "<h2 class='display-4 text-center border-left border-top border-danger p-3 rounded'> Last posted announces </h2>";
                echo "<div class='d-flex flex-wrap justify-content-center'>";
                foreach($announces as $announce) Announce::card($announce);
                echo "</div>";
            }else{
                ?>
                <div class="alert alert-warning" role="alert">
                    You must be <a href="/register" class="alert-link"> registered </a> and <a href="/login" class="alert-link"> logged in </a> to access the rest of the site.
                </div>
                <?php
            }
            ?>
        </div>
    </main>
    <?php include ROOT.'/footer.php' ?>
</body>
</html>