<?php

use Models\User;

if(!empty($_COOKIE['user'])){
    alert("You are already connected.", "warning", ["Back to home page" => '/home']);
    return;
}

$error = false;
$data = [];
$permissions = [];
if(!empty($_POST['register'])){
    if(!empty($_POST['first_name'])){
        $first_name = $_POST['first_name'];
        if(!empty($_POST['last_name'])){
            $last_name = $_POST['last_name'];
            if(!empty($_POST['email'])){
                $email = $_POST['email'];
                if(preg_match('/^\S+@\S+\.\S+$/',$email)){
                    if(!empty($_POST['password'])){
                        $password = $_POST['password'];
                        if(strlen($password) > 7){
                            if(isset($_POST['permission'])){
                                $permission = $_POST['permission'];
                                if(in_array($permission,['standard','announcer'])){
                                    $data['first_name'] = $first_name;
                                    $data['last_name'] = $last_name;
                                    $data['email'] = $email;
                                    $data['password'] = hash('sha256', $password);
                                    $permissions = $permission === 'standard' ? [1] : [1,2];
                                }else{
                                    $error = 'Your account type is not correct';
                                }
                            }else{
                                $error = 'You must choice a account type';
                            }
                        }else{
                            $error = 'You must enter a correct password (more of 7 characters)';
                        }
                    }else{
                        $error = 'You must enter a password';
                    }
                }else{
                    $error = 'You must enter a correct email';
                }
            }else{
                $error = 'You must enter your email';
            }
        }else{
            $error = 'You must enter your last name';
        }
    }else{
        $error = 'You must enter your first name';
    }
}

if(!empty($data)){
    if(!User::emailExists($data['email'])){
        if(User::register($data, $permissions)){
            alert('You are successfully registered !', 'success', ['Login' => '/login']);
            return;
        }else{
            $error = 'An error has occurred...';
        }
    }else{
        $error = 'You already have an account with the same email.';
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php include_once ROOT.'/head.php' ?>
    <title> AirBnB | Register </title>
</head>
<body class="d-flex justify-content-center align-items-center flex-column">
    <form action="/register" method="POST" class="d-flex flex-column align-items-center mt-3 w-50 h-50">
        <img src="/src/img/airbnb.svg" alt="logo" height="100"">
        <h1 class="display-3 mb-4 text-secondary"> Register </h1>
        <?php
            if($error){
                echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
            }
        ?>
        <input type="text" name="first_name" placeholder="First name" class="rounded shadow border-0 p-2 m-1" autofocus <?php value('first_name') ?>>
        <input type="text" name="last_name" placeholder="Last name" class="rounded shadow border-0 p-2 m-1" <?php value('last_name') ?>>
        <input type="email" name="email" placeholder="Email" class="rounded shadow border-0 p-2 m-1" <?php value('email') ?>>
        <input type="password" name="password" placeholder="Password" class="rounded shadow border-0 p-2 m-1" <?php value('password') ?>>
        <p class="mt-3"> What type of account do you need ? </p>
        <div class="d-flex justify-content-center">
            <label class="mx-2">
                <input type="radio" name="permission" value="announcer"> Announcer <span class="badge badge-info">10$ / month</span>
            </label>
            <label class="mx-2">
                <input type="radio" name="permission" value="standard" checked> Customer <span class="badge badge-success">100% free</span>
            </label>
        </div>
        <label>
            <input type="checkbox" name="" required>
            I accept that you take money from me
        </label>
        <input type="submit" name="register" value="Register" class="btn btn-primary m-2">
    </form>
    <a class="btn btn-secondary text-light" href="/login"> Already have account </a>
</body>
</html>