<?php

use Models\User;
use Models\Contract;
use Models\Reservation;
use Core\Database;

if(!isset($_SESSION['user'])){
    alert("You must be connected.", 'warning', ['Login' => '/login']);
    return;
}

if(empty($_GET['id'])){
    $_GET['id'] = $_COOKIE['user'];
}

if(!User::getBy(intval($_GET['id']))){
    alert("Unknown user...", 'danger', ['Back to home page' => '/home']);
    return;
}

$is_own_profile = $_COOKIE['user'] === $_GET['id'];
$user_id = intval($_GET['id']);
$user = User::getBy($user_id);
$reservations = User::getReservations($user_id);
$announces = User::getAnnounces($user_id);
$is_announcer = in_array('announcer', $user['permissions']);
$announcer_reservations = [];
$favorites = [];
if($is_announcer){
    $announcer_reservations = User::getCustomerReservations($user_id);
}
if($is_own_profile){
    $favorites = User::getFavorites($user_id);
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php include_once ROOT.'/head.php' ?>
    <title> Profile of <?php echo full_name($user) ?> </title>
</head>
<body class="min-vh-100 d-flex flex-column">
    <?php include ROOT.'/header.php' ?>
    <main class="flex-grow-1">
        <div class="container d-flex flex-column align-items-center">
            <img class="img-thumbnail mt-5 shadow" src="<?php rand_image('cat'); ?>" alt="avatar" width="150">
            <h1 class="text-center m-4 display-4"><?php echo full_name($user) ?></h1>
            <div class="d-flex flex-wrap w-100">
                <?php

                $table = [];
                foreach($user as $prop => $value){
                    if(!in_array($prop, ['password','id'])){
                        $resolved_prop = strpos($prop,'_timestamp') === false ? $prop : str_replace('timestamp','at',$prop);
                        $resolved_prop = beautify($resolved_prop);
                        $resolved_value = strpos($prop,'_timestamp') === false ? $value : date('l jS \of F Y', strtotime($value));
                        if(is_array($resolved_value)) $resolved_value = join(', ', $resolved_value);
                        $table[] = [
                            $resolved_prop => true,
                            $resolved_value => false
                        ];
                    }
                }
                card('Information', $table);

                if($is_own_profile){
                    $table = [[
                        'Price' => true,
                        'Title' => true,
                        'City' => true,
                        'Period' => true,
                        'Announcer' => true,
                        'Reserved places' => true,
                        '' => true
                    ]];
                    foreach($reservations as $reservation){
                        $table[] = [
                            $reservation['price'].'$/month' => false,
                            '<a href="/detail?id='.$reservation['announce_id'].'" class=\'text-primary\'>'.$reservation['title'].'</a>' => false,
                            $reservation['country'].'/'.$reservation['city'] => false,
                            period($reservation) => false,
                            full_name($reservation) => false,
                            $reservation['requested_places'].' on '.$reservation['places'] => false,
                            "<a href='/delete?type=reservation&id={$reservation['id']}' class='text-danger'> abort </a>" => false
                        ];
                    }
                    card('My reservations', $table, 'search');

                    $table = [];
                    foreach($favorites as $favorite){
                        $table[] = [
                            "<a href='/announce?id={$favorite['announce_id']}' class='text-primary'> {$favorite['title']} </a>" => false,
                            "<a href='/delete?type=favorite&id={$favorite['id']}' class='text-danger'> abort </a>" => false
                        ];
                    }
                    card('Favorites', $table);
                }

                if($is_announcer){
                    $table = [[
                        'Price' => true,
                        'Title' => true,
                        'City' => true,
                        'Places' => true,
                        '' => true
                    ]];
                    foreach($announces as $announce){
                        $table[] = [
                            $announce['price'].'$/month' => false,
                            '<a href="/detail?id='.$announce['id'].'" class=\'text-primary\'>'.$announce['title'].'</a>' => false,
                            $announce['country'].'/'.$announce['city'] => false,
                            $announce['places'] => false,
                            "<a href='/delete?type=announce&id={$announce['id']}' class='text-danger'> remove </a>" => false
                        ];
                    }
                    card(($is_own_profile ? 'My a' : 'A').'nnounces', $table, 'add');

                    $table = [[
                        'Announce' => true,
                        'Customer' => true,
                        'Period' => true,
                        'Reserved places' => true,
                        ' ' => false,
                        '' => false
                    ]];
                    foreach($announcer_reservations as $reservation){
                        $table[] = [
                            "<a href='/announce?id={$reservation['announce_id']}' class='text-primary'> {$reservation['title']} </a>" => false,
                            "<a href='/profile?id={$reservation['customer_id']}' class='text-primary'> ".full_name($reservation)." </a>" => false,
                            period($reservation) => false,
                            $reservation['requested_places'].' on '.$reservation['places'] => false,
                            "<a href='/delete?type=reservation&id={$reservation['id']}&contract=1' class='text-success'> accept </a>" => false,
                            "<a href='/delete?type=reservation&id={$reservation['id']}' class='text-danger'> reject </a>" => false
                        ];
                    }
                    card('Customer reservations', $table);
                }
                ?>
                <div class="d-flex justify-content-center p-3 w-100">
                    <a class="btn btn-danger text-light" href="/delete?type=profile&id=<?php echo $user_id ?>"> Remove </a>
                </div>
            </div>
        </div>
    </main>
    <?php include ROOT.'/footer.php' ?>
</body>
</html>
