<?php

use Models\Announce;

if(empty($_COOKIE['user'])){
    alert("You must be logged in.", "warning", ["Login" => '/login', 'Register' => '/register']);
    return;
}

$results = [];
if(!empty($_GET['search'])){
    $results = Announce::search($_GET['search']);
}

$page_index = isset($_GET['page']) ? intval($_GET['page']) : 0;

$item_per_page = 10;
$page_count = ceil(count($results)/$item_per_page);

$current_page = array_slice($results,10 * $page_index,10);

?><!doctype html>
<html lang="en">
<head>
    <?php include_once ROOT.'/head.php' ?>
    <title> AirBnB | Home </title>
</head>
<body class="min-vh-100 d-flex flex-column">
    <?php include ROOT.'/header.php' ?>
    <main class="flex-grow-1">
        <div class="container">
            <?php
            if(!empty($_GET['search'])){
                ?>
                <h1 class="display-3 mt-3 text-center"> Search : <?php echo beautify($_GET['search']) ?> </h1>
                <div class="d-flex flex-wrap">
                <?php
                if(count($current_page) > 0){
                    foreach($current_page as $announce) Announce::card($announce);
                }else{
                    echo '<h3 class="w-100 text-center text-muted"> No result </h3>';
                }
                echo '</div>';
            }else{
                ?>
                <div class="d-flex align-items-center justify-content-center flex-column">
                    <form method="get">
                        <input type="search" name="search" class="border-0 rounded-pill shadow-lg p-2 text-center mt-5 display-4" placeholder="Search on announces">
                    </form>
                </div>
                <?php
            }
            ?>
        </div>
    </main>
    <?php include ROOT.'/footer.php' ?>
</body>
</html>
