<?php

use Models\Announce;
use Models\User;
use Models\Reservation;
use Models\Favorite;
use Core\Database;

if(!isset($_SESSION['user'])){
    alert("You must be connected.", 'warning', ['Login' => '/login']);
    return;
}

if(empty($_GET['id']) || !Database::exists('announce', intval($_GET['id']))){
    alert("Announce not found...", 'warning', ['Back to home' => '/home']);
    return;
}

$announce = Announce::get(intval($_GET['id']));
$announcer = User::getBy(intval($announce['announcer_id']));
$is_own_announce = $announce['announcer_id'] === $_SESSION['user']['id'];

if(!$is_own_announce && isset($_GET['favorite'])){
    $success = Favorite::add([
        'user_id' => $_SESSION['user']['id'],
        'announce_id' => $announce['id']
    ]);
    if($success){
        alert("Favorite has been added", 'success', ['Back to announce' => '/announce?id='.$announce['id']]);
    }else{
        alert("Failed to add favorite...", 'danger', ['Back to announce' => '/announce?id='.$announce['id']]);
    }
    return;
}

$is_reserved = Database::has("SELECT * FROM reservation WHERE user_id = {$_SESSION['user']['id']} AND announce_id = {$announce['id']}");
$is_contracted = Database::has("SELECT * FROM contract WHERE user_id = {$_SESSION['user']['id']} AND announce_id = {$announce['id']}");

$error = null;
if(isset(
    $_POST['requested_places'],
    $_POST['start_timestamp'],
    $_POST['end_timestamp']
)){
    if(intval($_POST['requested_places']) > intval($announce['places'])){
        $error = "There are not enough places";
    }else{
        if(strtotime($_POST['start_timestamp']) >= strtotime($_POST['end_timestamp'])){
            $error = "Your reservation period is invalid";
        }else{
            $safe = true;
            foreach($announce['contracts'] as $contract){
                if(
                    (strtotime($_POST['start_timestamp']) > $contract['start_timestamp'] && strtotime($_POST['start_timestamp']) < $contract['end_timestamp']) ||
                    (strtotime($_POST['end_timestamp']) > $contract['start_timestamp'] && strtotime($_POST['end_timestamp']) < $contract['end_timestamp'])
                ){
                    $safe = false;
                }
            }
            if(!$safe){
                $error = "Your reservation period overlaps an existing contract";
            }else{
                $data = [
                    'requested_places' => $_POST['requested_places'],
                    'start_timestamp' => $_POST['start_timestamp'],
                    'end_timestamp' => $_POST['end_timestamp'],
                    'announce_id' => $announce['id'],
                    'user_id' => $_SESSION['user']['id']
                ];
                $success = Reservation::add($data);
                if($success){
                    alert("Your reservation has been sent", 'success', ['Back to announce' => '/announce?id='.$announce['id']]);
                    return;
                }else{
                    $error = "An error has occurred";
                }
            }
        }
    }
}

?><!doctype html>
<html lang="en">
<head>
    <?php include_once ROOT.'/head.php' ?>
    <title> <?php echo $announce['title'] ?> </title>
</head>
<body class="min-vh-100 d-flex flex-column">
<?php include ROOT.'/header.php' ?>
<main class="flex-grow-1">
    <div class="container ">
        <h1 class="text-center m-4 display-4"><?php echo $announce['title'] ?></h1>
        <h2 class="text-center text-muted"> <?php echo $announce['price'] ?>$ / month </h2>
        <div class="d-flex align-items-start">
            <div class="card m-2">
                <div class="card-header">
                    <h2> Photo </h2>
                </div>
                <img class="card-img-bottom" src="<?php rand_image(); ?>" alt="photo">
            </div>
            <div class="d-flex flex-wrap">
                <?php
                if($is_own_announce){
                    ?>
                    <div class="card m-2">
                        <div class="card-header">
                            <h2> Reservations </h2>
                        </div>
                        <div class="card-body">
                            <?php
                            if(count($announce['reservations']) > 0){
                                echo '<ul class="card-text">';
                                foreach($announce['reservations'] as $reservation){
                                    echo "<li> For {$reservation['requested_places']} places, from "._date($reservation['start_timestamp'])." to "._date($reservation['end_timestamp'])." </li>";
                                }
                                echo '</ul>';
                            }else{
                                echo "No reservation";
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }else{
                    ?>
                    <div class="card m-2">
                        <div class="card-header">
                            <h2> Owner </h2>
                        </div>
                        <div class="card-body">
                            <a href="/profile?id=<?php echo $announcer['id'] ?>" class="d-flex flex-column align-items-center">
                                <img class="img-thumbnail" src="<?php rand_image('cat'); ?>" alt="avatar" width="100">
                                <h5> <?php echo full_name($announcer) ?> </h5>
                                <p> <?php echo $announcer['email'] ?> </p>
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="card m-2">
                    <div class="card-header">
                        <h2> Description </h2>
                    </div>
                    <div class="card-body">
                        <div class="card-text">
                            <p><?php echo $announce['description'] ?></p>
                        </div>
                    </div>
                </div>
                <div class="card m-2">
                    <div class="card-header">
                        <h2> Location </h2>
                    </div>
                    <div class="card-body">
                        <div class="card-text">
                            <p><?php echo $announce['city'].' / '.$announce['country'] ?></p>
                        </div>
                    </div>
                </div>
                <div class="card m-2">
                    <div class="card-header">
                        <h2> Equipments </h2>
                    </div>
                    <div class="card-body">
                        <?php
                        if(count($announce['equipments']) > 0){
                            echo "<ul class=\"card-text\">";
                            foreach($announce['equipments'] as $equipment){
                                echo "<li>{$equipment['name']} <span class='badge badge-info'>x{$equipment['quantity']}</span></li>";
                            }
                            echo "</ul>";
                        }else{
                            echo "Apartment to furnish";
                        }
                        ?>

                    </div>
                </div>
                <div class="card m-2">
                    <div class="card-header">
                        <h2> Detail </h2>
                    </div>
                    <div class="card-body">
                        <div class="card-text">
                            <table>
                                <tr>
                                    <th class="px-3"> Type </th>
                                    <td class="px-3"><?php echo $announce['type'] ?></td>
                                </tr>
                                <tr>
                                    <th class="px-3"> Surface </th>
                                    <td class="px-3"><?php echo $announce['surface'] ?></td>
                                </tr>
                                <tr>
                                    <th class="px-3"> Places </th>
                                    <td class="px-3"><?php echo $announce['places'] ?></td>
                                </tr>
                                <tr>
                                    <th class="px-3"> Animals </th>
                                    <td class="px-3"><?php echo boolval($announce['authorize_animals']) ? 'Authorised' : 'Not allowed' ?></td>
                                </tr>
                                <tr>
                                    <th class="px-3"> Smokers </th>
                                    <td class="px-3"><?php echo boolval($announce['authorize_smokers']) ? 'Authorised' : 'Not allowed' ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card m-2">
                    <div class="card-header">
                        <h2> Contracts </h2>
                    </div>
                    <div class="card-body">
                        <?php
                        if(count($announce['contracts']) > 0){
                            echo "<ul class=\"card-text\">";
                            foreach($announce['contracts'] as $contract){
                                echo "<li>".period($contract)."</li>";
                            }
                            echo "</ul>";
                        }else{
                            echo "Apartment to furnish";
                        }
                        ?>
                    </div>
                </div>
                <?php
                if(!$is_own_announce){
                    ?>
                    <div class="card m-2">
                        <div class="card-header">
                            <h2> Book me </h2>
                        </div>
                        <div class="card-body">
                            <?php
                            if($is_contracted){
                                echo "You have already contracted this announce";
                            }else if($is_reserved) {
                                echo "You have already booked this announce";
                            }else{
                                if($error){
                                    echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
                                }
                                ?>
                                <form method="post" class="d-flex align-items-center flex-column">
                                    <div>
                                        <label>
                                            From: <input type="date" name="start_timestamp" class="form-control" <?php value('start_timestamp'); ?> min="<?php echo date('Y-m-d',time()) ?>" required>
                                        </label>
                                        <label>
                                            To: <input type="date" name="end_timestamp" class="form-control" <?php value('end_timestamp'); ?> min="<?php echo date('Y-m-d',time()) ?>" required>
                                        </label>
                                    </div>
                                    <label>
                                        Requested places: <input type="number" name="requested_places" class="form-control" required <?php value('requested_places'); ?>>
                                    </label>
                                    <div class="btn-group">
                                        <a href="/announce?id=<?php echo $announce['id'].'&favorite=1' ?>" class="btn btn-secondary"> Add to favorites </a>
                                        <input type="submit" value="Ok" class="btn btn-primary">
                                    </div>
                                </form>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</main>
<?php include ROOT.'/footer.php' ?>
</body>
</html>