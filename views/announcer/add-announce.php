<?php

use Models\Announce;

if(empty($_COOKIE['user'])){
    alert("You must be logged in.", "warning", ["Login" => '/login', 'Register' => '/register']);
    return;
}

if(!in_array('announcer', $_SESSION['user']['permissions'])){
    alert('You must have an announcer account', 'warning', ['Logout' => '/logout','Home' => '/home']);
    return;
}

$error = false;
if(isset($_POST['add'])){
    $data = $_POST;
    unset($data['add']);
    $data['announcer_id'] = $_SESSION['user']['id'];
    $data['authorize_animals'] = intval(isset($data['authorize_animals']));
    $data['authorize_smokers'] = intval(isset($data['authorize_smokers']));
    $announce_id = Announce::add($data);
    if($announce_id){
        alert('Your announce has been saved', 'success', ['Check' => '/detail?id='.$announce_id, 'Home' => '/home']);
        return;
    }else{
        $error = 'A problem has occurred';
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <?php include_once ROOT.'/head.php' ?>
    <title> AirBnB | Post </title>
</head>
<body class="d-flex justify-content-center align-items-center flex-column">
    <form id="add" action="/add" method="POST" class="d-flex align-items-center flex-column mt-3 w-50 h-50">
        <img class="text-center" src="/src/img/airbnb.svg" alt="logo" height="100"><br>
        <h1 class="display-3 mb-4 text-secondary text-center"> Add announce </h1>
        <?php
        if($error){
            echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
        }
        input('text', 'title');
        input('text','city');
        input('text', 'country');
        input('text', 'surface');
        input('number', 'price');
        input('number', 'places');
        input('radio', 'type', [
            'whole', 'private', 'collocation'
        ]);
        input('checkbox', 'authorize_animals');
        input('checkbox', 'authorize_smokers');
        input('textarea', 'description', ['form'=>'add']);
        input('textarea', 'equipments', [
            'form'=>'add',
            'notice' => 'Please enter the equipment available to the tenant, like this: <br><code> 3 chairs, 5 whiskey glasses, 2 dining tables </code> (etc...)'
        ]);
        input('submit', 'add');
        ?>
    </form>
</body>
</html>