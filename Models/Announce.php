<?php

namespace Models;

use Core\Database;

abstract class Announce extends Database
{
    public static function delete( int $id ): void
    {
        self::query("DELETE FROM announce WHERE id = $id");
    }

    public static function add( array $data ): int
    {
        $equipments = $data['equipments'];
        unset($data['equipments']);
        $success = self::insert('announce', $data);
        if(!$success) return 0;
        $announce_id = self::connexion()->lastInsertId();
        $equipments = array_map( function($equipment)use($announce_id){
            return [
                'quantity' => intval($equipment),
                'announce_id' => $announce_id,
                'name' => trim(preg_replace('/\d+/','',$equipment))
            ];
        }, preg_split('/,/', $equipments));
        foreach($equipments as $equipment){
            if($equipment['quantity'] > 0) self::insert('equipment', $equipment);
        }
        return $announce_id;
    }

    public static function get( int $id ): ?array
    {
        $announce = self::fetch('SELECT * FROM announce WHERE id = '.$id);
        if(!$announce) return null;

        $announce['contracts'] = self::fetchAll('SELECT * FROM contract WHERE announce_id = '.$id);
        $announce['reservations'] = self::fetchAll('SELECT * FROM reservation WHERE announce_id = '.$id);
        $announce['equipments'] = self::fetchAll('SELECT * FROM equipment WHERE announce_id = '.$id);

        return $announce;
    }

    public static function search( $keyword ): array
    {
        $announces = self::fetchAll("SELECT * FROM airbnb.announce");
        $results = [];
        foreach($announces as $announce){
            foreach($announce as $prop => $value){
                if(is_string($value) && includes($keyword,$value)){
                    $results[] = $announce;
                    break;
                }
            }
        }
        return $results;
    }

    public static function getLastAnnounces( int $limit = 1 ): array
    {
        return self::fetchAll('
            SELECT *
            FROM airbnb.announce
            ORDER BY created_timestamp DESC
            LIMIT '.$limit
        );
    }

    public static function card( array $announce ): void
    {
        ?>
        <div class="card m-1">
            <div class="card-body d-flex flex-column">
                <h5 class="card-title"> <?php echo htmlspecialchars($announce['title']) ?> </h5>
                <h6 class="card-subtitle mb-2 text-muted"> <?php echo htmlspecialchars($announce['country']).' - '.htmlspecialchars($announce['city']) ?> </h6>
                <p class="card-text flex-grow-1"> <?php echo htmlspecialchars($announce['description']) ?> </p>
                <img class="card-img-top" src="<?php rand_image() ?>" alt="photo">
                <a href="/detail?id=<?php echo $announce['id'] ?>" class="card-link bg-primary rounded-bottom text-center text-light py-1"> See more </a>
            </div>
        </div>
        <?php
    }
}