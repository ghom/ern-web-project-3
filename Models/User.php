<?php

namespace Models;

use Core\Database;

abstract class User extends Database
{
    public static function delete( int $id ): void
    {
        self::query("DELETE FROM airbnb.user WHERE id = $id");
    }

    public static function getBy( $value, string $by = 'id', string $quotes = '' ): ?array
    {
        $user = self::fetch('
            SELECT *
            FROM airbnb.user
            WHERE '.$by.' = '.$quotes.$value.$quotes
        );
        if(!$user) return null;

        $permissions = self::fetchAll('
            SELECT pt.name
            FROM airbnb.permission p
            LEFT JOIN permission_type pt on p.type_id = pt.id
            WHERE p.user_id = '.$user['id']
        );

        $user['permissions'] = array_map(function($permission){
            return $permission['name'];
        }, $permissions);

        return $user;
    }

    public static function emailExists(string $email ): bool
    {
        return self::has("SELECT email FROM user WHERE email = '$email'");
    }

    public static function register(array $user, array $permissions): bool
    {
        $success = self::insert('user', $user);
        $user_id = self::connexion()->lastInsertId();

        foreach($permissions as $permission){
            if($success){
                $success = self::insert('permission', [
                    'type_id' => $permission,
                    'user_id' => $user_id
                ]);
            }
        }

        return $success;
    }

    public static function getFavorites( int $id ): ?array
    {
        return Database::fetchAll("
            SELECT f.id, f.announce_id, a.title
            FROM favorite f
            LEFT JOIN user u on f.user_id = u.id
            LEFT JOIN announce a on f.announce_id = a.id
            WHERE u.id = $id
            GROUP BY f.id
        ");
    }

    public static function getCustomerReservations( int $id ): ?array
    {
        return Database::fetchAll("
            SELECT r.id, r.user_id, r.announce_id, r.start_timestamp, r.end_timestamp, r.created_timestamp, r.requested_places, a.id as announce_id, a.title, c.id as customer_id, c.first_name, c.last_name, a.places
            FROM reservation r
            LEFT JOIN announce a on r.announce_id = a.id
            LEFT JOIN user u on a.announcer_id = u.id
            LEFT JOIN user c on r.user_id = c.id
            WHERE u.id = $id
            GROUP BY r.id
        ");
    }

    public static function getReservations( int $id ): ?array
    {
        return self::fetchAll('
            SELECT 
                r.id,
                r.announce_id,
                r.requested_places,
                r.start_timestamp,
                r.end_timestamp,
                r.created_timestamp,
                a.price,
                a.title,
                a.description,
                a.type,
                a.city,
                a.country,
                a.places,
                u.first_name,
                u.last_name         
            FROM airbnb.reservation r
            LEFT JOIN announce a on r.announce_id = a.id
            LEFT JOIN user u on a.announcer_id = u.id
            WHERE r.user_id = '.$id
        );
    }

    public static function getAnnounces( int $id ): ?array
    {
        return self::fetchAll('
            SELECT * FROM airbnb.announce
            WHERE announcer_id = '.$id
        );
    }
}
