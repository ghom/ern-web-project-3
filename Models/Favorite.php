<?php


namespace Models;


use Core\Database;

class Favorite extends Database
{
    public static function delete( int $id ): void
    {
        self::query("DELETE FROM favorite WHERE id = $id");
    }

    public static function get( $id ): ?array
    {
        return self::fetch('SELECT * FROM favorite WHERE id = '.$id);
    }

    public static function add( $data ): bool
    {
        return self::insert('favorite', $data);
    }
}