<?php


namespace Models;

use Models\Reservation;
use Models\Announce;
use Core\Database;

class Contract extends Database
{
    public static function delete( int $id ): void
    {
        self::query("DELETE FROM contract WHERE id = $id");
    }

    public static function get( int $id ): ?array
    {
        return self::fetch('SELECT * FROM contract WHERE id = '.$id);
    }

    public static function add( array $data ): bool
    {
        $announce = Announce::get($data['announce_id']);
        foreach($announce['reservations'] as $reservation){
            if(
                (strtotime($data['start_timestamp']) > $reservation['start_timestamp'] && strtotime($data['start_timestamp']) < $reservation['end_timestamp']) ||
                (strtotime($data['end_timestamp']) > $reservation['start_timestamp'] && strtotime($data['end_timestamp']) < $reservation['end_timestamp'])
            ){
                Reservation::delete(intval($reservation['id']));
            }
        }
        return self::insert('contract', $data);
    }
}