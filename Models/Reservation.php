<?php

namespace Models;

use Core\Database;

abstract class Reservation extends Database
{
    public static function delete( int $id ): void
    {
        self::query("DELETE FROM reservation WHERE id = $id");
    }

    public static function get( $id ): ?array
    {
        return self::fetch('
            SELECT r.id, r.user_id, r.announce_id, r.start_timestamp, r.end_timestamp, r.created_timestamp, r.requested_places, a.announcer_id
            FROM reservation r
            LEFT JOIN announce a on r.announce_id = a.id
            WHERE r.id = '.$id.'
            GROUP BY r.id'
        );
    }

    public static function add( $data ): bool
    {
        return self::insert('reservation', $data);
    }
}

